## 🌟 What is it

**Teanologi Project:** Green Tea Responsive Landing.

-   **Home Made:** Kamus Bahasa Inggris Indonesia ini
merupakan Kamus Online yang dibuat
untuk memudahkan pencarian,
penggunaan dan pembacaan arti kata..
-   **A Best Choice:** Stevia,Chamomile,Teh Hijau,Hibiscus
-   **Sale** 45 % OFF In Appril 2022
-   **Contact** software.
## 💻 Demo function
-   ![Home](/Images/demo.png)
-   ![Home](/Images/demo2.png)
## 🧱 Technology Stack and Requirements
-   [Javascript](https://www.w3schools.com/js/) language
-   [Html](https://www.w3schools.com/html/) / [Css](https://www.w3schools.com/css/)
-   [Bootstrap](https://getbootstrap.com/)

